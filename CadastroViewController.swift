//
//  CadastroViewController.swift
//  Coopanest.CE
//
//  Created by Vandecarlos  Santana on 15/11/18.
//  Copyright © 2018 Vandecarlos  Santana. All rights reserved.
//

import UIKit
import FirebaseAuth
//import FirebaseDatabase
import Firebase
import CoreData


class CadastroViewController: UIViewController {
    

    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var senha: UITextField!
    var dominio = "@coopanest-ce.com.br"
    
    var auth: Auth!
   // var database : Database!
    var db: Firestore!
 
    var context: NSManagedObjectContext!
    
    func exibirMensagem (titulo:String, menssagem:String){
        let alerta = UIAlertController(title: titulo, message: menssagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction(title: "ok", style: .default, handler: nil)
        alerta.addAction(acaoCancelar)
        present(alerta, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        // Do any additional setup after loading the view.
        let doutorImagem = UIImage(named: "Icon-27")
        addLeftImage(txtField: login, andImage:doutorImagem!)
        
        let senhaImagem = UIImage(named: "Icon-29")
        addLeftImage(txtField: senha, andImage: senhaImagem!)
        
        //configurar aobjeto de autenticacao
        self.auth = Auth.auth()
        
        //Ativar dados OffLine
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        
        self.db = Firestore.firestore()
        self.db.settings = settings
        //fim dados offline
        
        //self.database = Database.database()
        
    }
    //cadastro usuario
    @IBAction func cadastrar(_ sender: Any) {
        
        let progressHUD = ProgressHUD(text: "Verificando ...")
        self.view.addSubview(progressHUD)
        
       
        
        if let loginR = self.login.text {
            if let senhaR = self.senha.text{
                let compSenha:String = "0000-";
                let senhaB = Base64().codificarStringBase64(texto: senhaR)
                //
                if let urlLog = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/login/"+loginR+"/"+senhaB){
                    let tarefaLog = URLSession.shared.dataTask(with: urlLog) { (dadosLog, requisicaoLog, erroLog) in
                        if(erroLog == nil){
                            if let dadosRetornoLog = dadosLog {
                                do{
                                    if let objetoJsonLog = try JSONSerialization.jsonObject(with: dadosRetornoLog, options:[]) as?
                                        [String: Any] {
                                        
                                        let login = objetoJsonLog["login"] as! String
                                        
                                          if (login != "ERROR USUARIO INVALIDO"){
                                          if let medico = objetoJsonLog["medico"] as? [String: Any]{
                                           
                                            DispatchQueue.main.async(execute: {
                                                
                                                //cria usuario firebase
                                                self.auth.createUser(withEmail: loginR+self.dominio , password: compSenha + senhaR, completion: {(usuario,erro) in
                                                    
                            
                                                   
                                                    if (erro == nil) {
                                                        //processos api firebase
                                                        let chave = FirebaseUtil().recuperaUid()
                                                        var usuario: Dictionary<String, String> = [:]
                                                        usuario["login"] = loginR
                                                        usuario["senha"] = senhaB //Base64().codificarStringBase64(texto:senhaR)
                                                        usuario["email"] = loginR+self.dominio
                                                        usuario["nome"] = medico["nome"] as? String
                                                        usuario["servico"] = medico["servico"] as? String
                                                        usuario["crm"] = medico["crm"] as? String
                                                        
                                                        
                                                        //grava usuario firebase
                                                       // let usuarios = self.database.reference().child("usuarios")
                                                       // usuarios.child(chave).setValue(usuario)
                                                        
                                                        self.db.collection("usuarios").document(chave).setData(usuario) { err in
                                                                if let err = err {
                                                                    print("Error adding document: \(err)")
                                                                } else {
                                                                    print("Document added with ID:" )
                                                                }
                                                        }
                                                        
                                                        //grava local dados
                                                        let entityName:String  = "Usu"
                                                        let requisicao = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                                                        do {
                                                            
                                                            let predicate = NSPredicate(format: "login == %@", loginR)
                                                            requisicao.predicate = predicate
                                                            
                                                            let hospitais = try self.context.fetch(requisicao)
                                                            
                                                            if hospitais.count > 0 {
                                                              
                                                                let requisicaoInsert = NSEntityDescription.insertNewObject(forEntityName: entityName, into: self.context)
                                                                
                                                                do {
                                                                    requisicaoInsert.setValue(loginR , forKey: "login")
                                                                    requisicaoInsert.setValue(senhaB, forKey: "senha")
                                                                    requisicaoInsert.setValue(medico["nome"] as? String, forKey: "nome")
                                                                    requisicaoInsert.setValue(medico["servico"] as? String, forKey: "servico")
                                                                    requisicaoInsert.setValue(medico["crm"] as? String, forKey: "crm")
                                                                    
                                                                    try self.context.save()
                                                                    
                                                                }catch{
                                                                    print("Erro ao Atualiza Suspensao codigo:\(loginR)")
                                                                }
                                                                
                                                            }else{
                                                                
                                                                let requisicaoInsert = NSEntityDescription.insertNewObject(forEntityName: entityName, into: self.context)
                                                                
                                                                do {
                                                                    requisicaoInsert.setValue(loginR , forKey: "login")
                                                                    requisicaoInsert.setValue(senhaB, forKey: "senha")
                                                                    requisicaoInsert.setValue(medico["nome"] as? String, forKey: "nome")
                                                                    requisicaoInsert.setValue(medico["servico"] as? String, forKey: "servico")
                                                                    requisicaoInsert.setValue(medico["crm"] as? String, forKey: "crm")
                                                                    
                                                                    try self.context.save()
                                                                    
                                                                }catch{
                                                                    print("Erro ao Atualiza Suspensao codigo:\(loginR)")
                                                                }
                                                                
                                                            }
                                                            
                                                        }catch{
                                                            print("Erro na busca do codigo:\(loginR)")
                                                        }
                                                        //fim grava local
                                                        /*
                                                        
                                                        if let url = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/atualizaExtrato/"+loginR+"/"+senhaB+"/"+chave){
                                                            let tarefa = URLSession.shared.dataTask(with: url) { (dados, requisicao, erro) in
                                                                if erro == nil {
                                                                    if let dadosRetorno = dados {
                                                                        do{
                                                                            
                                                                            if let objetoJson = try JSONSerialization.jsonObject(with: dadosRetorno, options:[]) as?
                                                                                [String: Any] {
                                                                                if (objetoJson["status"] as? [String: Any]) != nil{
                                                                                    
                                                                                   //DispatchQueue.main.async(execute: {
                                                                                    //print ("estatus cadastro : \(processo)")
                                                                       
                                                                                }
                                                                            }
                                                                        }catch{
                                                                            //Fazer Correcao no processo
                                                                            print("erro processos extrato")
                                                                        }
                                                                    }//fim dadosRetorno
                                                                }
                                                            }
                                                            tarefa.resume()
                                                        }//fim url processo
                                                        
                                                        if let url = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/atualizaProcedimento/"+loginR+"/"+senhaB+"/"+chave){
                                                            let tarefa = URLSession.shared.dataTask(with: url) { (dados, requisicao, erro) in
                                                                if erro == nil {
                                                                    if let dadosRetorno = dados {
                                                                        do{
                                                                            
                                                                            if let objetoJson = try JSONSerialization.jsonObject(with: dadosRetorno, options:[]) as?
                                                                                [String: Any] {
                                                                                if (objetoJson["status"] as? [String: Any]) != nil{
                                                                                    
                                                                                   print ("Requisitou a atualizacao Procedimentos" )
                                                                                    
                                                                                }
                                                                            }
                                                                        }catch{
                                                                            //Fazer Correcao no processo
                                                                            print("erro processos procedimento")
                                                                        }
                                                                    }//fim dadosRetorno
                                                                }
                                                            }
                                                            tarefa.resume()
                                                        }//fim url processo
                                                        
                                                        if let url = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/atualizaHospital/"+loginR+"/"+senhaB+"/"+chave){
                                                            let tarefa = URLSession.shared.dataTask(with: url) { (dados, requisicao, erro) in
                                                                if erro == nil {
                                                                    if let dadosRetorno = dados {
                                                                        do{
                                                                            
                                                                            if let objetoJson = try JSONSerialization.jsonObject(with: dadosRetorno, options:[]) as?
                                                                                [String: Any] {
                                                                                if (objetoJson["status"] as? [String: Any]) != nil{
                                                                                    
                                                                                    //DispatchQueue.main.async(execute: {
                                                                                   // print ("estatus cadastro : \(processo)")
                                                                                    
                                                                                }
                                                                            }
                                                                        }catch{
                                                                            //Fazer Correcao no processo
                                                                            print("erro processos hospital")
                                                                        }
                                                                    }//fim dadosRetorno
                                                                }
                                                            }
                                                            tarefa.resume()
                                                        }//fim url processo
                                                        
                                                        if let url = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/atualiza/"+loginR+"/"+senhaB+"/"+chave){
                                                            let tarefa = URLSession.shared.dataTask(with: url) { (dados, requisicao, erro) in
                                                                if erro == nil {
                                                                    if let dadosRetorno = dados {
                                                                        do{
                                                                            
                                                                            if let objetoJson = try JSONSerialization.jsonObject(with: dadosRetorno, options:[]) as?
                                                                                [String: Any] {
                                                                                if (objetoJson["status"] as? [String: Any]) != nil{
                                                                                    print ("Requisitou a atualizacao Guias" )
                                                                                }
                                                                            }
                                                                        }catch{
                                                                            //Fazer Correcao no processo
                                                                            print("erro processos guias")
                                                                        }
                                                                    }//fim dadosRetorno
                                                                }
                                                            }
                                                            tarefa.resume()
                                                        }//fim url processo
                                                        
                                                        if let url = URL(string: "https://us-central1-guias-c0041.cloudfunctions.net/app/atualizaConvenio/"+loginR+"/"+senhaB+"/"+chave){
                                                            let tarefa = URLSession.shared.dataTask(with: url) { (dados, requisicao, erro) in
                                                                if erro == nil {
                                                                    if let dadosRetorno = dados {
                                                                        do{
                                                                            
                                                                            if let objetoJson = try JSONSerialization.jsonObject(with: dadosRetorno, options:[]) as?
                                                                                [String: Any] {
                                                                                if (objetoJson["status"] as? [String: Any]) != nil{
                                                                                    
                                                                                    //DispatchQueue.main.async(execute: {
                                                                                   // print ("estatus cadastro : \(processo)")
                                                                                    
                                                                                }
                                                                            }
                                                                        }catch{
                                                                            //Fazer Correcao no processo
                                                                            print("erro processos convenio")
                                                                        }
                                                                    }//fim dadosRetorno
                                                                }
                                                            }
                                                            tarefa.resume()
                                                        }//fim url processo
                                                        */
                                                        
                                                    }else{
                                                        self.exibirMensagem(titulo: "Ops!", menssagem: "Erro no registro! Usuário já REGISTRADO ou comunicação FALHOU! ")
                                                        //print(erro)
                                                    }
                                                    
                                                })//FimUser firebase
                                            
                                            })//fim DispatchQueue.main.async
                                            
                                            }else{
                                                self.exibirMensagem(titulo: "Ops!", menssagem: " Usuário ou Senha inválido ...")
                                                print(objetoJsonLog)
                                            }
                                            
                                          }else{
                                            self.exibirMensagem(titulo: "Ops!", menssagem: " Usuário ou Senha inválido ...")
                                            print(objetoJsonLog)
                                         }
                                         
                                    }
                                }catch{
                                    self.exibirMensagem(titulo: "Ops!", menssagem: "Erro Usúario ou Senha, Verifique e tente novamente!")
                                    print("Erro no servico, Tente novamente!")
                                }
                            }// dadosRetornoLog
                        }
                    }
                    tarefaLog.resume()
                }//urlLog
                //
                
               
                
            }else{
                self.exibirMensagem(titulo: "Ops!", menssagem: "Digite a senha do sistema de gestao!")
            }
            
        }else{
            self.exibirMensagem(titulo: "Ops!", menssagem: "Digite o usuário do sistema de gestao!")
        }
        
        self.view.backgroundColor = UIColor.black
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    func addLeftImage(txtField: UITextField, andImage img: UIImage){
        
        let cgrectCGRect = CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height)
        let leftImageView = UIImageView(frame: cgrectCGRect)
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
        
    }
    
    //procedimento
    func gravaProcedimento(){
        
    }
    
    

}




//recupera convenio firebase
/* let fireConvenio = self.database.reference().child("convenio")
 
 fireConvenio.observe(DataEventType.value, with: {(dados) in
 
 let retorno = dados.value as! NSDictionary
 
 for (key, value) in retorno {
 
 //print("Value: \(value) for key: \(key)")
 let dvalor = value as? NSDictionary
 
 //Salvar convenio local
 let convenio = NSEntityDescription.insertNewObject(forEntityName: "Convenio", into: self.context)
 
 convenio.setValue(dvalor?["codigo"] , forKey: "codigo")
 convenio.setValue(dvalor?["nome"], forKey: "descricao")
 
 do {
 try self.context.save()
 print("Convenio salvos")
 }catch{
 print("erro ao salvar convenio")
 }
 }
 
 
 })*/


//recupera hospital firebase
/*  let fireHospital = self.database.reference().child("hospital")
 fireHospital.observe(DataEventType.value, with: {(dados) in
 
 let retorno = dados.value as! NSDictionary
 
 for (key, value) in retorno {
 
 //print("Value: \(value) for key: \(key)")
 let dvalor = value as? NSDictionary
 
 //Salvar convenio local
 let convenio = NSEntityDescription.insertNewObject(forEntityName: "Hospital", into: self.context)
 
 convenio.setValue(dvalor?["codigo"] , forKey: "codigo")
 convenio.setValue(dvalor?["nome"], forKey: "descricao")
 
 do {
 try self.context.save()
 print("Hospital salvos")
 }catch{
 print("erro ao salvar Hospital")
 }
 }
 
 
 })*/
